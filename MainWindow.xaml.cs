﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.NetworkInformation;
using System.ComponentModel;

namespace BackgroundTrafficMeter
{
    public class Item
    {
        public string id;
        public string name;
        public Item(string id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Capture cap = new Capture();
        private double speed = 0;
        int timeInt = 5;
        string interfaceId = null;
        Dictionary<string,HelpContext> help = null;
        string HelpFile = "assets/HelpFile.json";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var a = cap.getStats();
            if (speed <= 0)
                return;
            set_gauge_values(brTotal, lbTotal,a.TotalCtr,0.3);
            set_gauge_values(brBroadcast, lbBroadcast, a.BroadcastCtr,0.01);
            set_gauge_values(brMulticast, lbMulticast, a.MulticastCtr,0.1);
            set_gauge_values(brGarbage, lbGarbage, a.GarbageCtr,0.001);
            string toptalkers = "";
            foreach(KeyValuePair<string, int> b in a.TopTalkers)
            {
                toptalkers += b.Key;
                toptalkers += ": \t";
                toptalkers += FrmtNwSpeed((b.Value * 8) / ((double)timeInt));
                toptalkers += "\n";
            }
            top_talkers.Text = toptalkers;
            updateNetworkInterface();
        }
        private void set_gauge_values(ProgressBar p, Label l, Int64 value,double warning)
        {
            double bps = (value*8) / ((double)timeInt);
            if (bps > (speed * warning))
                p.Foreground = Brushes.Red;
            else
                p.Foreground = Brushes.LightGreen;
            l.Content = FrmtNwSpeed(bps);
            p.Value=bps / speed * 100;
            if (p.Value > 0 && p.Value < 1)
                p.Value = 1;
        }
        private void ComboBox_Initialized_1(object sender, EventArgs e)
        {
            ((ComboBox)sender).ItemsSource = NetworkInterface.GetAllNetworkInterfaces();
        }
        private void NetworkInterfaceChanged(object sender, EventArgs e)
        {
            var i = (NetworkInterface)((ComboBox)sender).SelectedItem;
            interfaceId = i.Id;
            updateNetworkInterface();
            try
            {
                cap.StartCapture(interfaceId);
            }
            catch(System.DllNotFoundException)
            {
                MessageBox.Show("NPCAP Library not found on your system");
            }
        }
        private void updateNetworkInterface()
        {
            foreach(NetworkInterface i in NetworkInterface.GetAllNetworkInterfaces())
            {
                if( i.Id == interfaceId)
                {
                    lbInterfaceDetail.Content = string.Format("Details:\nStatus: {0}\nSpeed:{1}", i.OperationalStatus.ToString(), FrmtNwSpeed(i.Speed));
                    speed = i.Speed;
                    return;
                }
            }
        }
        private string FrmtNwSpeed(double s)
        {
            string end = "bps";
            if (s < 1024)
                return string.Format("{0}{1}", s.ToString("0.###"), end);
            s= s/1024;
            end = "kbps";
            if (s < 1024)
                return string.Format("{0}{1}", s.ToString("0.###"), end);
            s = s / 1024;
            end = "Mbps";
            if (s < 1024)
                return string.Format("{0}{1}", s.ToString("0.###"), end);
            s = s / 1024;
            end = "Gbps";
            return string.Format("{0}{1}", s.ToString("0.###"), end);
        }

        private void StartUpdateDataTimer(object sender, RoutedEventArgs e)
        {
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, timeInt);
            dispatcherTimer.Start();
        }
        private void close(object sender, CancelEventArgs e)
        {
            cap.StopCapture();
        }

        private void bg_MouseUp(object sender, MouseButtonEventArgs e)
        {
            InfoWindow info = new InfoWindow();
            info.setData(help.GetValueOrDefault( ((Label)sender).ContentStringFormat , new HelpContext()));
                info.Show();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();
            if(args.Contains<string>("build_help"))
            {
                help = HelpProvider.loadFromFile(HelpFile);
                this.Close();
            }
            help = HelpProvider.loadFromresources();
        }
    }
}
