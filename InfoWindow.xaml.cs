﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BackgroundTrafficMeter
{
    /// <summary>
    /// Interaction logic for InfoWindow.xaml
    /// </summary>
    public partial class InfoWindow : Window
    {
        HelpContext data = null;
        public InfoWindow()
        {
            InitializeComponent();
        }
        public void setData(HelpContext a)
        {
            string links = "<ul>";
            if(a.links!=null)
                foreach(var b in a.links)
                {
                    string title = "";
                    if (b.title != null)
                        title = b.title;
                    links += string.Format("<li>{0}<a target='_blank' href='{1}'>{1}</a></li>", title, b.url);
                }
            links += "</ul>";
            string html = string.Format("<html><body style='{{font-family: Tahoma, Arial, Sans-Serif;}}'><h1>{0}</h1>{1}{2}</body></html>", a.title, a.text,links);
            webcontent.NavigateToString(html);
            data = a;
        }

        private void closeWindow(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
