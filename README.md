# BackgroundTrafficMeter
## first preview release
[download Version 0.0.1](http://www.murxs.ch/pub/backgroundtrafficmeter/BackgroundTrafficMeter.zip)

Needs [npcap](https://nmap.org/npcap/) or [Wireshark](https://www.wireshark.org/download.html) to be installed on the system.

![screenshot](https://gitlab.com/shrank/backgroundtrafficmeter/uploads/76f1405d28fcba3ca3837844de9d7caa/screenshot.jpg)
