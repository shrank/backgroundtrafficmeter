﻿using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackgroundTrafficMeter
{
    class CaptureStats
    {
        public Int64 TotalCtr = 0;
        public Int64 GarbageCtr = 0;
        public Int64 BroadcastCtr = 0;
        public Int64 MulticastCtr = 0;
        public List<KeyValuePair<string, int>> TopTalkers=null;
        private Dictionary<string, int> topTalkers=null;
        public CaptureStats()
        {
            topTalkers = new Dictionary<string, int>();
        }
        public void addToptalker(string ip,int bytes)
        {
            lock(topTalkers)
            {
                try
                {
                    topTalkers[ip] += bytes;
                }
                catch (Exception e)
                {
                    topTalkers.Add(ip, bytes);
                }
            }
        }
        public List<KeyValuePair<string, int>> getTopTalkers(int len)
        {
            List<KeyValuePair<string, int>> top = new List<KeyValuePair<string, int>>();
            lock (topTalkers)
            {
                foreach (KeyValuePair<string, int> entry in topTalkers)
                {
                    int i = 0;
                    bool done = false;
                    foreach (KeyValuePair<string, int> a in top)
                    {
                        if (entry.Value > a.Value)
                        {
                            top.Insert(i, entry);
                            done = true;
                            break;
                        }
                        i++;
                        if (i >= len)
                        {
                            done = true;
                            break;
                        }
                    }
                    if (done == true)
                        continue;
                    if (top.Count < len)
                        top.Add(entry);
                }
                topTalkers.Clear();
            }
            if(top.Count>len)
                top.RemoveRange(len - 1, top.Count-len);
            return top;
        }
    }
    class Capture
    {
        CaptureStats stats;
        PcapDevice dev = null;
        public Capture()
        {
            stats = new CaptureStats();
        }
        public void StartCapture(string Interface)
        {
            StopCapture();
            foreach(var a in LibPcapLiveDeviceList.Instance)
            {
                if (a.Interface.Name.Contains(Interface))
                {
                    dev = a;
                    break;
                }
            }
            if (dev == null)
                return;
            dev.Open();
            dev.Filter = "ether dst host not " + dev.Interface.MacAddress.ToString();
            dev.OnPacketArrival += this.Device_OnPacketArrival;
            dev.StartCapture();
        }
        public void Device_OnPacketArrival(object s, CaptureEventArgs e)
        {
            stats.TotalCtr += e.Packet.Data.Length;
            var packet = PacketDotNet.Packet.ParsePacket(e.Packet.LinkLayerType, e.Packet.Data);
            var ether = packet.Extract<PacketDotNet.EthernetPacket>();
            if (ether.DestinationHardwareAddress.ToString().StartsWith("FFFFFFFFFFFF"))
                stats.BroadcastCtr += e.Packet.Data.Length;
            else if ("13579BDF".Contains(ether.DestinationHardwareAddress.ToString().Substring(1, 1)))
                stats.MulticastCtr += e.Packet.Data.Length;
            else if(!ether.SourceHardwareAddress.ToString().Equals(dev.Interface.MacAddress.ToString()))
                stats.GarbageCtr += e.Packet.Data.Length;
            var ip = packet.Extract<PacketDotNet.IPPacket>();
            if (ip == null)
                stats.addToptalker(ether.SourceHardwareAddress.ToString(), e.Packet.Data.Length);
            else
                stats.addToptalker(ip.SourceAddress.ToString(), e.Packet.Data.Length);
        }
        public void StopCapture()
        {
            if (dev != null)
            {
                dev.StopCapture();
                dev = null;
            }
        }
        public CaptureStats getStats()
        {
            var res = new CaptureStats();
            res.BroadcastCtr = stats.BroadcastCtr;
            stats.BroadcastCtr = 0;
            res.MulticastCtr = stats.MulticastCtr;
            stats.MulticastCtr = 0;
            res.TotalCtr = stats.TotalCtr;
            stats.TotalCtr = 0;
            res.GarbageCtr = stats.GarbageCtr;
            stats.GarbageCtr = 0;
            res.TopTalkers = stats.getTopTalkers(10);
            return res;
        }
    }
}
