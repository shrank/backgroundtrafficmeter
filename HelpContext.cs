﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Web;
using System.Resources;
using System.Reflection;

namespace BackgroundTrafficMeter
{
    public class WikiResponse
    {

        public WikiQuery query { get; set; }
    }
    public class WikiQuery
    {
        public Dictionary<string, WikiPage> pages { get; set; }
    }
    public class WikiPage
    {
        public string title { get; set; }
        public string extract { get; set; }
        public int pageid { get; set; }
        public string link { get; set; }
    }
    public class HelpContext
    {
        public string title { get; set; }
        public string wikiKeyword { get; set; }
        public string text { get; set; }
        public List<HelpLink> links { get; set; }
    }

    public class HelpLink
    {
        public string title { get; set; }
        public string url { get; set; }
    }
    class HelpProvider
    {
        private static readonly HttpClient client = new HttpClient();
        public static Dictionary<string,HelpContext> loadFromFile(string fileName)
        {
            string jsonString = File.ReadAllText(fileName);
            var data = JsonSerializer.Deserialize<Dictionary<string,HelpContext>>(jsonString);
            foreach(string a in data.Keys)
            {
                if(data[a].wikiKeyword!=null && data[a].wikiKeyword.Length > 0)
                {
                    var res= HelpProvider.getWikiPage(data[a].wikiKeyword);
                    data[a].text =string.Format("<p><small>from wikipedia:</small><br/>{0}</p>", res.extract.Trim());

                }
            }
            File.WriteAllText("help.json", JsonSerializer.Serialize(data));
            return data;
        }

        private static WikiPage getWikiPage(string topic)
        {
            string url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=" + HttpUtility.UrlEncode(topic);
            var res = client.GetStringAsync(url).Result;
            var response = JsonSerializer.Deserialize<WikiResponse>(res);
            var k = response.query.pages.GetEnumerator();
            k.MoveNext();
            return k.Current.Value;
        }

        public static Dictionary<string, HelpContext> loadFromresources()
        {
            string jsonString = "";
            var assembly = Assembly.GetExecutingAssembly();
            var values= assembly.GetManifestResourceNames();
            using (Stream stream = assembly.GetManifestResourceStream("BackgroundTrafficMeter.help.json"))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    jsonString = reader.ReadToEnd();
                }

            }
            return JsonSerializer.Deserialize<Dictionary<string, HelpContext>>(jsonString);
        }

    }
}
